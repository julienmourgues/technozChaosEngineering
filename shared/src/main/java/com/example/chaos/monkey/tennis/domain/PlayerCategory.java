package com.example.chaos.monkey.tennis.domain;

/**
 *  @author Julien Mourgues
 */
public enum PlayerCategory {
    MALE,FEMALE,YOUNG;
}
