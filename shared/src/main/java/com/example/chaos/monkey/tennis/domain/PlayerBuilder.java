package com.example.chaos.monkey.tennis.domain;

public class PlayerBuilder {
    private long id;
    private String firstname;
    private String lastname;
    private PlayerCategory category;

    public PlayerBuilder setId(long id) {
        this.id = id;
        return this;
    }

    public PlayerBuilder setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public PlayerBuilder setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public PlayerBuilder setCategory(PlayerCategory category) {
        this.category = category;
        return this;
    }

    public Player createPlayer() {
        return new Player(id, firstname, lastname, category);
    }
}