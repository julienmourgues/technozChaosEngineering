package com.example.chaos.monkey.tennis.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Julien Mourgues
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Player {

    private long id;
    private String firstname;
    private String lastname;
    private PlayerCategory category;

}
