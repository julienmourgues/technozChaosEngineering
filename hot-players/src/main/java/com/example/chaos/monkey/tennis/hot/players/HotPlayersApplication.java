package com.example.chaos.monkey.tennis.hot.players;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotPlayersApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotPlayersApplication.class, args);
	}
}
