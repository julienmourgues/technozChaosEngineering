package com.example.chaos.monkey.tennis.hot.players;


import com.example.chaos.monkey.tennis.domain.Player;
import com.example.chaos.monkey.tennis.domain.PlayerCategory;
import com.example.chaos.monkey.tennis.domain.PlayerBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/hot")
public class HotPlayersRestController {

    @GetMapping("/players")
    public List<Player> getHotPlayers() {
        AtomicLong aLong = new AtomicLong(7);

        PlayerBuilder playerBuilder = new PlayerBuilder();

        Player player1 = playerBuilder.setCategory(PlayerCategory.MALE).setId(aLong.getAndIncrement()).setFirstname("Novak").setLastname("Djokovic")
                .createPlayer();

        Player player2 = playerBuilder.setCategory(PlayerCategory.YOUNG).setId(aLong.getAndIncrement()).setFirstname("Dominic").setLastname("Thiem")
                .createPlayer();

        Player player3 = playerBuilder.setCategory(PlayerCategory.FEMALE).setId(aLong.getAndIncrement()).setFirstname("Ashleigh").setLastname("Barty")
                .createPlayer();
        return Arrays.asList(player1, player2, player3);
    }

}
