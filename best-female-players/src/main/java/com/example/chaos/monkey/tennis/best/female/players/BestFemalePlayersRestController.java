package com.example.chaos.monkey.tennis.best.female.players;

import com.example.chaos.monkey.tennis.domain.Player;
import com.example.chaos.monkey.tennis.domain.PlayerCategory;
import com.example.chaos.monkey.tennis.domain.PlayerBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Julien Mourgues
 */
@RestController
@RequestMapping("/bestfemale")
public class BestFemalePlayersRestController {

    @GetMapping("/players")
    public List<Player> getBestFemalePlayers() {
        AtomicLong aLong = new AtomicLong(1);

        PlayerBuilder playerBuilder = new PlayerBuilder();

        Player player1 = playerBuilder.setCategory(PlayerCategory.FEMALE).setId(aLong.getAndIncrement()).setFirstname("Steffi").setLastname("Graf")
                .createPlayer();

        Player player2 = playerBuilder.setCategory(PlayerCategory.FEMALE).setId(aLong.getAndIncrement()).setFirstname("Serena").setLastname("Williams")
                .createPlayer();

        Player player3 = playerBuilder.setCategory(PlayerCategory.FEMALE).setId(aLong.getAndIncrement()).setFirstname("Martina").setLastname("Navrátilová")
                .createPlayer();
        return Arrays.asList(player1, player2, player3);
    }

}
