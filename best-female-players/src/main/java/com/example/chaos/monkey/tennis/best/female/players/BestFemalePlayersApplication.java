package com.example.chaos.monkey.tennis.best.female.players;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BestFemalePlayersApplication {

	public static void main(String[] args) {
		SpringApplication.run(BestFemalePlayersApplication.class, args);
	}
}
