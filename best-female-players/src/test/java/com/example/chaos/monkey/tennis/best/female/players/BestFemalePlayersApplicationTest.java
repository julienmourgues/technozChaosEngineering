package com.example.chaos.monkey.tennis.best.female.players;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Julien Mourgues
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BestFemalePlayersApplicationTest {

    @Test
    public void contextLoads() {
    }

}