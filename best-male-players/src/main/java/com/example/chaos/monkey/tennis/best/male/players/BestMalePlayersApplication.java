package com.example.chaos.monkey.tennis.best.male.players;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BestMalePlayersApplication {

	public static void main(String[] args) {
		SpringApplication.run(BestMalePlayersApplication.class, args);
	}
}
