package com.example.chaos.monkey.tennis.best.male.players;

import com.example.chaos.monkey.tennis.domain.Player;
import com.example.chaos.monkey.tennis.domain.PlayerCategory;
import com.example.chaos.monkey.tennis.domain.PlayerBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Julien Mourgues
 */
@RestController
@RequestMapping("/bestmale")
public class BestMalePlayersRestController {

    @GetMapping("/players")
    public List<Player> getBestMalePlayers() {
        AtomicLong aLong = new AtomicLong(4);

        PlayerBuilder playerBuilder = new PlayerBuilder();

        Player player1 = playerBuilder.setCategory(PlayerCategory.MALE).setId(aLong.getAndIncrement()).setFirstname("Roger").setLastname("Federer")
                .createPlayer();

        Player player2 = playerBuilder.setCategory(PlayerCategory.MALE).setId(aLong.getAndIncrement()).setFirstname("Rafael").setLastname("Nadal")
                .createPlayer();

        Player player3 = playerBuilder.setCategory(PlayerCategory.MALE).setId(aLong.getAndIncrement()).setFirstname("Novak").setLastname("Djokovic")
                .createPlayer();

        return Arrays.asList(player1, player2, player3);
    }
}
