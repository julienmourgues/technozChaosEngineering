package com.example.chaos.monkey.tennis.gateway.commands;

import com.example.chaos.monkey.tennis.domain.Player;
import com.example.chaos.monkey.tennis.gateway.domain.PlayerResponse;
import com.example.chaos.monkey.tennis.gateway.domain.ResponseType;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

/**
 * @author Julien Mourgues
 */
public class HotPlayersCommand extends HystrixCommand<PlayerResponse> {

    private final RestTemplate restTemplate;
    private final String url;

    public HotPlayersCommand(HystrixCommandGroupKey group, int timeout, RestTemplate restTemplate,
                             String url) {
        super(group, timeout);
        this.restTemplate = restTemplate;
        this.url = url;
    }

    protected PlayerResponse run() throws Exception {
        PlayerResponse response = new PlayerResponse();

        response.setPlayers(restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Player>>() {
        }).getBody());

        response.setResponseType(ResponseType.REMOTE_SERVICE);

        return response;
    }

    @Override
    protected PlayerResponse getFallback() {
        return new PlayerResponse(ResponseType.FALLBACK, Collections.<Player>emptyList());
    }
}
