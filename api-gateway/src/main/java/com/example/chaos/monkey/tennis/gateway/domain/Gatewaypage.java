package com.example.chaos.monkey.tennis.gateway.domain;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Julien Mourgues
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonPropertyOrder({ "bestMalePlayersResponse", "bestFemalePlayersResponse", "hotPlayersResponse", "duration", "statusBestMalePlayers","statusBestFemalePlayers","statusHotPlayers" })
public class Gatewaypage {

    private long duration;
    private String statusBestMalePlayers;
    private String statusBestFemalePlayers;
    private String statusHotPlayers;
    private PlayerResponse bestMalePlayersResponse;
    private PlayerResponse bestFemalePlayersResponse;
    private PlayerResponse hotPlayersResponse;
}
