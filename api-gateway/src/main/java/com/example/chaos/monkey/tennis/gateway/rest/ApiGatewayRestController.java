package com.example.chaos.monkey.tennis.gateway.rest;

import com.example.chaos.monkey.tennis.domain.Player;
import com.example.chaos.monkey.tennis.gateway.commands.BestFemalePlayersCommand;
import com.example.chaos.monkey.tennis.gateway.commands.BestMalePlayersCommand;
import com.example.chaos.monkey.tennis.gateway.commands.HotPlayersCommand;
import com.example.chaos.monkey.tennis.gateway.domain.PlayerResponse;
import com.example.chaos.monkey.tennis.gateway.domain.ResponseType;
import com.example.chaos.monkey.tennis.gateway.domain.Gatewaypage;
import com.netflix.hystrix.HystrixCommandGroupKey;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
public class ApiGatewayRestController {

    private RestTemplate restTemplate;

    @Value("${rest.endpoint.bestmaleplayers}")
    private String urlBestMalePlayers;

    @Value("${rest.endpoint.bestfemaleplayers}")
    private String urlBestFemalePlayers;

    @Value("${rest.endpoint.hotplayers}")
    private String urlHotPlayers;

    public ApiGatewayRestController() {
        this.restTemplate = new RestTemplateBuilder().build();
    }

    @GetMapping("/gatewaypage")
    public Gatewaypage getGatewaypage() {
        Gatewaypage page = new Gatewaypage();
        long start = System.currentTimeMillis();

        // Create Futures for requesting results
        Future<PlayerResponse> bestFemalePlayersFuture = getBestFemalePlayers();
        Future<PlayerResponse> bestMalePlayersFuture = getBestMalePlayers();
        Future<PlayerResponse> hotPlayersFuture = getHotPlayers();

        // Get Responses from Futures
        page.setBestMalePlayersResponse(extractResponse(bestMalePlayersFuture));
        page.setBestFemalePlayersResponse(extractResponse(bestFemalePlayersFuture));
        page.setHotPlayersResponse(extractResponse(hotPlayersFuture));

        // Summary
        page.setStatusBestMalePlayers(page.getBestMalePlayersResponse().getResponseType().name());
        page.setStatusBestFemalePlayers(page.getBestFemalePlayersResponse().getResponseType().name());
        page.setStatusHotPlayers(page.getHotPlayersResponse().getResponseType().name());

        // Request duration
        page.setDuration(System.currentTimeMillis() - start);
        return page;
    }

    private PlayerResponse extractResponse(Future<PlayerResponse> responseFuture) {
        try {
            return responseFuture.get();
        } catch (InterruptedException e) {
            return new PlayerResponse(ResponseType.ERROR, Collections.<Player>emptyList());
        } catch (ExecutionException e) {
            return new PlayerResponse(ResponseType.ERROR, Collections.<Player>emptyList());
        }
    }


    private Future<PlayerResponse> getHotPlayers() {
        return new HotPlayersCommand(HystrixCommandGroupKey.Factory.asKey("hotplayers"), 200, restTemplate, urlHotPlayers).queue();
    }

    private Future<PlayerResponse> getBestMalePlayers() {
        return new BestMalePlayersCommand(HystrixCommandGroupKey.Factory.asKey("bestMalePlayers"), 200, restTemplate, urlBestMalePlayers).queue();
    }

    private Future<PlayerResponse> getBestFemalePlayers() {
        return new BestFemalePlayersCommand(HystrixCommandGroupKey.Factory.asKey("bestFemalePlayers"), 200, restTemplate, urlBestFemalePlayers).queue();
    }

}
