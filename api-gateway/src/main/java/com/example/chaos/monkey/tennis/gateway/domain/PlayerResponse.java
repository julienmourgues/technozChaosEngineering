package com.example.chaos.monkey.tennis.gateway.domain;

import com.example.chaos.monkey.tennis.domain.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Julien Mourgues
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PlayerResponse {

    private ResponseType responseType;
    private List<Player> players;
}
