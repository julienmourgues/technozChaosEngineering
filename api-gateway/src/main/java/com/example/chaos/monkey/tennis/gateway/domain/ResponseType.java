package com.example.chaos.monkey.tennis.gateway.domain;

/**
 * @author Julien Mourgues
 */
public enum ResponseType {

    REMOTE_SERVICE, FALLBACK, ERROR;
}
