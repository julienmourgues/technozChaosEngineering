# chaos-monkey-spring-boot-demo
Démo de Chaos Monkey for Spring Boot

# URL d'accès aux API :
* cd best-male-players & mvn spring-boot:run -> "http://localhost:8082/bestmale/players/"
* cd best-female-players & mvn spring-boot:run -> "http://localhost:8084/bestfemale/players/"
* cd hot-players & mvn spring-boot:run -> "http://localhost:8083/hot/players/"
* cd api-gateway & mvn spring-boot:run -> "http://localhost:8080/gatewaypage/"

# Configuration Chaos Monkey pour demo :
* Activer par défaut sur hot-players, best-male-players et best-female-players
* Best-female-players : activation assault Latence, latencyRangeStart 100 ms, latencyRangeEnd 150ms, niveau 1 et activation assault Exception
* Best-male-players : activation assault Latence, latencyRangeStart 3000 ms, latencyRangeEnd 6000ms
* Hot-players : activation assault Latence, latencyRangeStart 100 ms, latencyRangeEnd 150ms, niveau 1 et activation assault AppKiller toutes les minutes